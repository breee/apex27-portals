$(document).ready(function(){

var images = ['header-1', 'header-2', 'header-3']
 $('.page-header').css({'background-image': 'url(assets/img/header/' + images[Math.floor(Math.random() * images.length)] + '.jpg)'})

 $('.new-properties > .tile-gallery').slick({
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  centerMode: true,
	  lazyLoad: 'ondemand',
	  speed: 600,
	  prevArrow: '#new-sale-prev',
	  nextArrow: '#new-sale-next',
	  swipe: false,
	  responsive: [
	    {
	      breakpoint: 1025,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	      }
	    },
	    {
	      breakpoint: 769,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	      }
	    },
	    {
	      breakpoint: 576,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
	      }
	    },
    ]
 })

 $('.new-let > .tile-gallery').slick({
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  centerMode: true,
	  lazyLoad: 'ondemand',
	  speed: 600,
	  prevArrow: '#new-let-prev',
	  nextArrow: '#new-let-next',
	  swipe: false,
	  responsive: [
	    {
	      breakpoint: 1025,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	      }
	    },
	    {
	      breakpoint: 769,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	      }
	    },
	    {
	      breakpoint: 576,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1,
	      }
	    },
    ]
 })

 $('.property > .tile-gallery').slick({
	  lazyLoad: 'ondemand',
	  speed: 600,
	  prevArrow: '#property-prev',
	  nextArrow: '#property-next',
	  swipe: false
 })

})
